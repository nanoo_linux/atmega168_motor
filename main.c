#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/delay.h>


#define NULL 0

enum Task_type {
	rs232
};

struct Task
{
	struct Task *next;
	enum Task_type receiver;
	uint8_t index_;
	char rs232_byte;
};

static volatile struct Task *queue_head = NULL;
static struct Task *queue_tail = NULL;


#define QUEUE_HEAP_SIZE 20
static uint8_t queue_allocated_index[QUEUE_HEAP_SIZE];
static struct Task queue_storage[QUEUE_HEAP_SIZE];


void queue_init(void)
{
	int i;
	for (i=0; i<QUEUE_HEAP_SIZE; ++i) {
		queue_allocated_index[i] = 0;
		queue_storage[i].index_ = i;
	}
}

struct Task *queue_malloc(void)
{
	int i=0;
	for (i=0; i<QUEUE_HEAP_SIZE; ++i) {
		if (queue_allocated_index[i] == 0) {
			queue_allocated_index[i] = 1;
			break;
		}
	}
	if (i == QUEUE_HEAP_SIZE) {
		return NULL;
	}
	return &(queue_storage[i]);
}

void queue_free(volatile struct Task *t)
{
	queue_allocated_index[t->index_] = 0;
}

void queue_post(struct Task *msg)
{
	if (queue_head == NULL) {
		queue_head = msg;
	} else {
		queue_tail->next = msg;
	}
	queue_tail = msg;
	queue_tail->next = NULL;
}

volatile struct Task *queue_wait(void)
{
	volatile struct Task *ret;
	while (queue_head == NULL) {
		sleep_mode();
	}
	ret = queue_head;
	queue_head = queue_head->next;
	return ret;
}

enum motor_state {
	MOTOR_ONE = 1,
	MOTOR_TWO = 2,
	MOTOR_THREE = 3,
	MOTOR_FOUR = 4
};

static const enum motor_state motor_states[] = {
	MOTOR_FOUR,
	MOTOR_ONE,
	MOTOR_TWO,
	MOTOR_THREE,
	MOTOR_FOUR,
	MOTOR_ONE
};

static const unsigned char cw_states[] = {0x14, 0x18, 0x24, 0x28};

void do_motor_position_one(void)
{
	PORTD = cw_states[0];
}

void do_motor_position_two(void)
{
	PORTD = cw_states[1];
}

void do_motor_position_three(void)
{
	PORTD = cw_states[2];
}

void do_motor_position_four(void)
{
	PORTD = cw_states[3];
}

static void (*motor_actions[])(void) = {
	0,
	do_motor_position_one,
	do_motor_position_two,
	do_motor_position_three,
	do_motor_position_four
};

int next_motor_state(int a)
{
	return motor_states[a+1];
}

int prev_motor_state(int a)
{
	return motor_states[a-1];
}

enum MotorDirection {
	MOTOR_DIRECTION_CW = 0,
	MOTOR_DIRECTION_CCW = 1
};

void do_step(enum MotorDirection a)
{
	static enum motor_state state = MOTOR_ONE;
	motor_actions[state]();
	_delay_ms(5);
	PORTD = 0;
	_delay_ms(5);
	if (a == MOTOR_DIRECTION_CW) {
		state = next_motor_state(state);
	} else {
		state = prev_motor_state(state);
	}
}

void rs232_send_ack(char ack)
{
	UDR0 = ack;
	while (( UCSR0A & (1 << TXC0 ) ) == 0) {}
}

void do_rs232(char byte)
{
	if (byte > 0) {
		while (byte-- > 0) {
			do_step(MOTOR_DIRECTION_CW);
		}
	} else {
		while (byte++ < 0) {
			do_step(MOTOR_DIRECTION_CCW);
		}
	}
	rs232_send_ack('0');
}

void rs232_setup(void)
{

#if 0
		#define FOSC 1843200 // Clock Speed
		#define BAUD 9600
		#define MYUBRR FOSC/16/BAUD-1
		UBRR0H = (unsigned char)(MYUBRR>>8);
		UBRR0L = (unsigned char)MYUBRR;
#endif

	// 9600
	UBRR0H = 0;
	UBRR0L = 104;
	// 8N1
	UCSR0C = 3 << UCSZ00;
	// engage
	UCSR0B = (1 << RXEN0) | (1 << RXCIE0) | (1 << TXEN0);
	return;
}

ISR (USART_RX_vect)
{
	//TODO: check status of receiver
	struct Task *t = queue_malloc();
	if (t != NULL) {
		t->receiver = rs232;
		t->rs232_byte = UDR0;
		queue_post(t);
	}
}

int main(void)
{
	DDRD = 0xff;
	queue_init();
	rs232_setup();
	sei();
	while (1) {
		volatile struct Task *msg = queue_wait();
		switch (msg->receiver) {
			case rs232:
				do_rs232(msg->rs232_byte);
				break;
			default:
				break;
		}
		queue_free(msg);
	}
	return 0;
}
