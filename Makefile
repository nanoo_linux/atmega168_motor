TARGET=main

CC=avr-gcc
CFLAGS=-Wall -O0 -DF_CPU=$(F_CPU) -mmcu=$(MCU)
MCU=atmega168
F_CPU=16000000UL

OBJCOPY=avr-objcopy
BIN_FORMAT=ihex

PORT=/dev/ttyUSB0
BAUD=19200
PROTOCOL=stk500v1
PART=$(MCU)
AVRDUDE=avrdude -F -V

RM=rm -f

.PHONY: all
all: $(TARGET).hex

$(TARGET).hex: $(TARGET).elf

$(TARGET).elf: $(TARGET).s

$(TARGET).s: $(TARGET).c

.PHONY: clean
clean:
	$(RM) $(TARGET).elf $(TARGET).hex $(TARGET).s

.PHONY: upload
up: $(TARGET).hex
	$(AVRDUDE) -c $(PROTOCOL) -p $(PART) -P $(PORT) -b $(BAUD) -U flash:w:$<

%.elf: %.s ; $(CC) $(CFLAGS) -s -o $@ $<

%.s: %.c ; $(CC) $(CFLAGS) -S -o $@ $<

%.hex: %.elf ; $(OBJCOPY) -O $(BIN_FORMAT) -R .eeprom $< $@
